function plot = plot_robotic_arm(T,scale)
    transform_matrix = eye(4);
    Old_Oa = zeros(4,1);
    for i = 1:length(T)
        transform_matrix = transform_matrix*T{i};
        Oa=transform_matrix*[0;0;0;1];
        xa=transform_matrix*[scale;0;0;1];
        ya=transform_matrix*[0;scale;0;1];
        za=transform_matrix*[0;0;scale;1];

        plot3([Oa(1) xa(1)],[Oa(2) xa(2)],[Oa(3) xa(3)],'Color','r','LineWidth',3.5);
        plot3([Oa(1) ya(1)],[Oa(2) ya(2)],[Oa(3) ya(3)],'Color','g','LineWidth',3.5);
        plot = plot3([Oa(1) za(1)],[Oa(2) za(2)],[Oa(3) za(3)],'Color','b','LineWidth',3.5);
        
        line([Old_Oa(1), Oa(1)],[Old_Oa(2), Oa(2)],[Old_Oa(3), Oa(3)], 'LineWidth',2);
        Old_Oa = Oa;
    end
end