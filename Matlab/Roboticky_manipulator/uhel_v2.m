first_joint = 50;
second_joint = 40;
third_joint = 30;
joint_width = 10;

w = figure();
n = 0
for n = 0:5:360
    clf(w);
    A = transformation(n, 0, 0, 20);
    B = transformation(0, first_joint, 0, 270);
    C = transformation(0, 0, joint_width, 90+n);
    D = transformation(0, second_joint, 0, 0);
    E = transformation(0, 0, joint_width, n);
    F = transformation(n, third_joint, 0, 0); 
    G = transformation(0, joint_width, 0, 270); 
    H = transformation(n, joint_width, 0, 90);
    Gripper = transformation(n, joint_width, 0, 0);
    
    hold on;
    grid on;
    axis equal;
    axis([-110 110 -110 110 0 160])
    plot_robotic_arm({A,B,C,D,E,F,G,H,Gripper},5);
    line([0,0],[0,0],[0,30]);
    %plot_frame(A,5);
    %plot_frame(A*B,5);
    %plot_frame(A*B*C,5);
    %plot_frame(A*B*C*D,5);
    %plot_frame(A*B*C*D*E,5);
    %plot_frame(A*B*C*D*E*F,5);

    hold off;
    view(3);
    pause(0.000001)
end