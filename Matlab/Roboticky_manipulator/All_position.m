first_joint = 50;
second_joint = 30;
third_joint = 20;
joint_width = 10;

step = 20;

w = figure();
hold on;
grid on;
axis equal;
axis([-70 70 -70 70 0 140])

n = 0;
B = transformation(0, first_joint, 0, 270);
D = transformation(0, 0, second_joint, 0);
G = transformation(0, 10, 0, 270);
Gripper = transformation(n, 10, 0, 0);



for first = 0:step:360
    A = transformation(first, 0, 0, 0);
    for second = 0:step:360 
        C = transformation(270+second, joint_width, 0, 0);
        for third = 0:step:360 
            E = transformation(90+third, joint_width, 0, 90);
            for fourth = 0%:step:360 
                 F = transformation(fourth, third_joint, 0, 0); 
                 for fifth = 0%:step:360 
                     H = transformation(180-second-third, -10, 0, 90);
                     plot_end({A,B,C,D,E,F,G,H,Gripper},2);
                 end
            end
        end
    end
end

hold off;
view(3);