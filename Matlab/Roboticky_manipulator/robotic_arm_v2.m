function slider
    first_joint = 50;
    second_joint = 30;
    third_joint = 20;
    joint_width = 10;
    A = transformation(0, 0, 0, 15);
    B = transformation(0, first_joint, 0, 270);
    C = transformation(0, 0, joint_width, 90);
    D = transformation(0, second_joint, 0, 0);
    E = transformation(0, 0, joint_width, 0);
    F = transformation(0, third_joint, 0, 0); 
    G = transformation(0, joint_width, 0, 270); 
    H = transformation(0, -joint_width, 0, 90);
    Gripper = transformation(0, joint_width, 0, 0);
    
    FigH = figure('position',[360 500 1200 800]);
    axes('units','pixels', ...
         'position',[50 50 700 700], 'NextPlot', 'add');
    grid on;
    axis equal;
    axis([-90 90 -90 90 0 120])
    %axis off;
    view(3);
    ROBOTIC_ARM = plot_robotic_arm({A,B,C,D,E,F,G,H,Gripper},5);
    
    uicontrol('style','text','position',[970 620 100 40],'FontSize',20,'String','1. joint');
    Text_First = uicontrol('style','text','position',[820 600 50 15],'String','0');
    Slider_First = uicontrol('style','slider','position',[900 600 250 20],'min', -360, 'max', 360, 'Value', 0);
    addlistener(Slider_First, 'Value', 'PostSet', @Function_First);
    
    function Function_First(source, eventdata)
        number = get(eventdata.AffectedObject, 'Value');
        cla(FigH);
        A = transformation(number, 0, 0, 15);
        ROBOTIC_ARM = plot_robotic_arm({A,B,C,D,E,F,G,H,Gripper},5);
        Text_First.String = num2str(number);
    end

    uicontrol('style','text','position',[970 520 100 40],'FontSize',20,'String','2. joint');
    Text_Second = uicontrol('style','text','position',[820 500 50 15],'String','0');
    Slider_Second = uicontrol('style','slider','position',[900 500 250 20],'min', -360, 'max', 360, 'Value', 0);
    addlistener(Slider_Second, 'Value', 'PostSet', @Function_Second);
    
    function Function_Second(source, eventdata)
        number = get(eventdata.AffectedObject, 'Value');
        cla(FigH);
        C = transformation(0, 0, joint_width, 90+number);
        ROBOTIC_ARM = plot_robotic_arm({A,B,C,D,E,F,G,H,Gripper},5);
        Text_Second.String = num2str(number);
    end
    
    uicontrol('style','text','position',[970 420 100 40],'FontSize',20,'String','3. joint');
    Text_Third = uicontrol('style','text','position',[820 400 50 15],'String','0');
    Slider_Third = uicontrol('style','slider','position',[900 400 250 20],'min', -360, 'max', 360, 'Value', 0);
    addlistener(Slider_Third, 'Value', 'PostSet', @Function_Third);
    
    function Function_Third(source, eventdata)
        number = get(eventdata.AffectedObject, 'Value');
        cla(FigH);
        E = transformation(0, 0, joint_width, number);
        ROBOTIC_ARM = plot_robotic_arm({A,B,C,D,E,F,G,H,Gripper},5);
        Text_Third.String = num2str(number);
    end

    uicontrol('style','text','position',[970 320 100 40],'FontSize',20,'String','4. joint');
    Text_Fourth = uicontrol('style','text','position',[820 300 50 15],'String','0');
    Slider_Fourth = uicontrol('style','slider','position',[900 300 250 20],'min', -360, 'max', 360, 'Value', 0);
    addlistener(Slider_Fourth, 'Value', 'PostSet', @Function_Fourth);
    
    function Function_Fourth(source, eventdata)
        number = get(eventdata.AffectedObject, 'Value');
        cla(FigH);
        F = transformation(number, third_joint, 0, 0); 
        ROBOTIC_ARM = plot_robotic_arm({A,B,C,D,E,F,G,H,Gripper},5);
        Text_Fourth.String = num2str(number);
    end

    uicontrol('style','text','position',[970 220 100 40],'FontSize',20,'String','5. joint');
    Text_Fifth = uicontrol('style','text','position',[820 200 50 15],'String','0');
    Slider_Fifth = uicontrol('style','slider','position',[900 200 250 20],'min', -360, 'max', 360, 'Value', 0);
    addlistener(Slider_Fifth, 'Value', 'PostSet', @Function_Fifth);
    
    function Function_Fifth(source, eventdata)
        number = get(eventdata.AffectedObject, 'Value');
        cla(FigH);
        H = transformation(number, -10, 0, 90);
        ROBOTIC_ARM = plot_robotic_arm({A,B,C,D,E,F,G,H,Gripper},5);
        Text_Fifth.String = num2str(number);
    end

    uicontrol('style','text','position',[970 120 100 40],'FontSize',20,'String','6. joint');
    Text_Sixth = uicontrol('style','text','position',[820 100 50 15],'String','0');
    Slider_Sixth = uicontrol('style','slider','position',[900 100 250 20],'min', -360, 'max', 360, 'Value', 0);
    addlistener(Slider_Sixth, 'Value', 'PostSet', @Function_Sixth);
    
    function Function_Sixth(source, eventdata)
        number = get(eventdata.AffectedObject, 'Value');
        cla(FigH);
        Gripper = transformation(number, 10, 0, 0);
        ROBOTIC_ARM = plot_robotic_arm({A,B,C,D,E,F,G,H,Gripper},5);
        Text_Sixth.String = num2str(number);
    end
    movegui(FigH, 'center')
end