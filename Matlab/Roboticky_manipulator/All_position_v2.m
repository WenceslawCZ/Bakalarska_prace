first_joint = 40;
second_joint = 30;
third_joint = 10;
joint_width = 10;

step = 30;

w = figure();
hold on;
grid on;
axis equal;
axis([-80 80 -80 80 0 140])

n = 0;
A = transformation(n, 0, 0, 15);
B = transformation(0, first_joint, 0, 270);
C = transformation(0, 0, joint_width, 90+n);
D = transformation(0, second_joint, 0, 0);
E = transformation(0, 0, joint_width, n);
F = transformation(n, third_joint, 0, 0); 
G = transformation(0, joint_width, 0, 270); 
H = transformation(n, -joint_width, 0, 90);
Gripper = transformation(n, joint_width, 0, 0);




for first = 0:step:360
    A = transformation(first, 0, 0, 20);
    for second = 0:step:360 
        C = transformation(0, 0, joint_width, 90+second);
        for third = 0:step:360 
            E = transformation(0, 0, joint_width, third);
            for fourth = 0%:step:360 
                 F = transformation(fourth, third_joint, 0, 0); 
                 for fifth = 0%:step:360 
                     H = transformation(180-second-third, -10, 0, 90);
                     plot_end({A,B,C,D,E,F,G,H,Gripper},2);
                 end
            end
        end
    end
end

hold off;
view(3);