function plot_frame(T,scale)

Oa=T*[0;0;0;1];
xa=T*[scale;0;0;1];
ya=T*[0;scale;0;1];
za=T*[0;0;scale;1];

plot3([Oa(1) xa(1)],[Oa(2) xa(2)],[Oa(3) xa(3)],'Color','r','LineWidth',3.5);
plot3([Oa(1) ya(1)],[Oa(2) ya(2)],[Oa(3) ya(3)],'Color','g','LineWidth',3.5);
plot3([Oa(1) za(1)],[Oa(2) za(2)],[Oa(3) za(3)],'Color','b','LineWidth',3.5);

end