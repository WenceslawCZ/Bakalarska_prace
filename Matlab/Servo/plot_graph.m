function graph = plot_graph(data1, data2,t)
    plot(t,data1,'Color','r','LineWidth',1);
    graph = plot(t,data2,'Color','b','LineWidth',1);
    %line([0 100],[220 220], 'LineWidth',2);
end