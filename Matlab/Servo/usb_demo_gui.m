%% Create GUI

buffer=[0;0]; % buffer for drawing
global h_plot buffer check_timer

h_figure=figure(1);
clf
h_figure.CloseRequestFcn=@close_fcn;

% gui elemetns
h_axes=axes('Position',[0.2 0.1 0.7 0.8],'YLim',[0 40],'XGrid','on','YGrid','on');
hold on
h_plot=plot(h_axes,buffer,'.');
h_slider=uicontrol('Style','slider','Units','Normalized','Position',[0.08 0.1 0.05 0.8],'Min',0,'Max',40,'value',20);
addlistener(h_slider,'ContinuousValueChange',@slider_callback);

%% Initialize USB Interface

USBDriver('callback_data_received',@data_received);
USBDriver('connect');
check_timer=timer('TimerFcn','USBDriver(''check'')','Period',0.05,'executionMode','fixedSpacing'); 
start(check_timer);

%% Callbacks

% receives and draws data
function data_received(data)
    global h_plot buffer
    
    znaky = string(char(typecast(data,'uint8')'));
    message = [znaky{:}];
    
    for i = 1:size(message,2)
        if (message(i) == ';')
            cropped_message = message(1:i-1)
        end
    end
    splited_data = split(cropped_message);
    angle = (360/(2^14 -1)) * str2double(splited_data(3))
    
    buffer=[0; angle];
    h_plot.YData=buffer;
    drawnow
end

% sends data from the slider
function slider_callback(obj,ev)
    USBDriver('send',uint32(obj.Value));
end

% disconnects when figure is closed
function close_fcn(obj,ev)
    global check_timer
    stop(check_timer)
    USBDriver('disconnect');
    delete(obj)
end