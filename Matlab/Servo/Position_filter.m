clear all
close all
data = importdata('150.txt');

figure('Renderer', 'painters', 'Position', [300 50 890 700])
hold on
grid on

data(:,4) = data(:,4) - data(1,4)*ones(length(data(:,4)),1);
data(:,4) = data(:,4)*(30.5*10^-6);
data(:,1) = (360/16383)*data(:,1);
data(:,2) = (360/16383)*data(:,2);
data(:,3) = (360/16383)*data(:,3);

plot(data(:,4), data(:,1),'LineWidth',1.2);
plot(data(:,4), data(:,2),'LineWidth',1);
plot(data(:,4), data(:,3),'LineWidth',1);

set(gca,'FontSize',16)
title("Měření nelinearity enkodéru při konstantní rychlosti");
xlabel('Čas  [s]');
ylabel('Rozdíl absolutních poloh enkodérů   [°]')
legend({'Úhel', 'Průměrovací filtr 1.řádu', 'Průměrovací filtr 2.řádu', 'Průměrovací filtr 4.řádu'},'Location','Northwest');



hold off