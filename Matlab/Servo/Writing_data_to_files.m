% Initialize USB Interface
USBDriver('callback_data_received',@data_received);
USBDriver('connect');
check_timer=timer('TimerFcn','USBDriver(''check'')','Period',0.05,'executionMode','fixedSpacing'); 
start(check_timer);

% Send data
USBDriver('send',uint32([3 1 4 1 5 9 2 6]));

global pwm;
pwm = 0;
global soubor;
soubor = fopen( 'results.txt', 'wt' );
% Callback
function data_received(data)
    %global soubor;
    global pwm;
    global soubor;
    znaky = string(char(typecast(data,'uint8')'));
    message = [znaky{:}];
    
    for i = 1:size(message,2)
        if (message(i) == ';')
            cropped_message = message(1:i-1)
        end
    end
    splited_data = split(cropped_message);
    
    if(str2double(splited_data(2)) ~= pwm)
        cesta = sprintf('%d.txt',str2double(splited_data(2)))
        %cesta = splited_data(2) + ".txt"
        fclose(soubor);
        soubor = fopen(cesta, 'wt');
        pwm = str2double(splited_data(2));
    end
    
    fprintf(soubor, '%d,%f\n', str2double(splited_data(3)), str2double(splited_data(5))/10);
    
    %angle = (360/(2^14 -1)) * str2double(splited_data(3))
    %compass(cos(deg2rad(angle)),sin(deg2rad(angle)))
end
