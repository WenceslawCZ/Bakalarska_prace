function slider
    angle = 0;
    RPM = 25;
    P = 25;
    I = 140;
    D = 1.2;

    % Initialize USB Interface
    USBDriver('callback_data_received',@data_received);
    USBDriver('connect');
    check_timer=timer('TimerFcn','USBDriver(''check'')','Period',0.05,'executionMode','fixedSpacing'); 
    start(check_timer);
    %USBDriver('send',int32([100*angle, 100*RPM, 100*P, 100*I, 100*D]));
    
    
    t = linspace(-15.25,0,500);
    buffer1=zeros(500,1);
    buffer2=ones(500,1); 
    FigH = figure('position',[360 500 1200 800]);
    FigH.CloseRequestFcn=@close_fcn;
    axes('units','pixels', ...
         'position',[80 60 680 680], 'NextPlot', 'add');
    xlabel('Čas   [s]','FontSize',14)
    ylabel('Úhel natočení serva   [°]','FontSize',14);
    grid on;
    xlim([t(1) t(end)])
    %axis equal;
    %axis([0 1000 -360 360])
    %axis off;
    
    ROBOTIC_ARM = plot_graph(buffer1, buffer2,t);
    
    uicontrol('style','text','position',[970 620 100 40],'FontSize',20,'String','Úhel serva');
    Text_First = uicontrol('style','text','position',[820 600 50 20],'String',string(angle),'FontSize',14);
    Slider_First = uicontrol('style','slider','position',[900 600 250 20],'min', -360, 'max', 360, 'Value', angle);
    addlistener(Slider_First, 'Value', 'PostSet', @Function_First);
    
    function Function_First(source, eventdata)
        angle = get(eventdata.AffectedObject, 'Value');
        Text_First.String = num2str(angle);
        USBDriver('send',int32([100*angle, 100*RPM, 100*P, 100*I, 100*D]));
    end

    uicontrol('style','text','position',[970 520 100 40],'FontSize',20,'String','RPM');
    Text_Second = uicontrol('style','text','position',[820 500 50 20],'String',string(RPM),'FontSize',14);
    Slider_Second = uicontrol('style','slider','position',[900 500 250 20],'min', 0, 'max', 25, 'Value', RPM);
    addlistener(Slider_Second, 'Value', 'PostSet', @Function_Second);
    
    function Function_Second(source, eventdata)
        RPM = get(eventdata.AffectedObject, 'Value');
        Text_Second.String = num2str(RPM);
        USBDriver('send',int32([100*angle, 100*RPM, 100*P, 100*I, 100*D]));
    end
    
    uicontrol('style','text','position',[970 420 100 40],'FontSize',20,'String','P');
    Text_Third = uicontrol('style','text','position',[820 400 50 20],'String',string(P),'FontSize',14);
    Slider_Third = uicontrol('style','slider','position',[900 400 250 20],'min', 0, 'max', 100, 'Value', P);
    addlistener(Slider_Third, 'Value', 'PostSet', @Function_Third);
    
    function Function_Third(source, eventdata)
        P = get(eventdata.AffectedObject, 'Value');
        Text_Third.String = num2str(P);
        USBDriver('send',int32([100*angle, 100*RPM, 100*P, 100*I, 100*D]));
    end

    uicontrol('style','text','position',[970 320 100 40],'FontSize',20,'String','I');
    Text_Fourth = uicontrol('style','text','position',[820 300 50 20],'String',string(I),'FontSize',14);
    Slider_Fourth = uicontrol('style','slider','position',[900 300 250 20],'min', 0, 'max', 1000, 'Value', I);
    addlistener(Slider_Fourth, 'Value', 'PostSet', @Function_Fourth);
    
    function Function_Fourth(source, eventdata)
        I = get(eventdata.AffectedObject, 'Value');
        Text_Fourth.String = num2str(I);
        USBDriver('send',int32([100*angle, 100*RPM, 100*P, 100*I, 100*D]));
    end
    
    uicontrol('style','text','position',[970 220 100 40],'FontSize',20,'String','D');
    Text_Fifth = uicontrol('style','text','position',[820 200 50 20],'String',string(D),'FontSize',14);
    Slider_Fifth = uicontrol('style','slider','position',[900 200 250 20],'min', 0, 'max', 5, 'Value', D);
    addlistener(Slider_Fifth, 'Value', 'PostSet', @Function_Fifth);
    
    function Function_Fifth(source, eventdata)
        D = get(eventdata.AffectedObject, 'Value');
        Text_Fifth.String = num2str(D);
        USBDriver('send',int32([100*angle, 100*RPM, 100*P, 100*I, 100*D]));
    end
    

    

    function data_received(data)
        data
        cla(FigH);
        buffer1=[buffer1(2:end);(360.0/16383.0)*double(data(1))];
        buffer2=[buffer2(2:end);(360.0/16383.0)*double(data(2))];
        ROBOTIC_ARM = plot_graph(buffer1, buffer2, t);
    end
    
    function close_fcn(obj,ev)
        stop(check_timer)
        USBDriver('disconnect');
        delete(obj)
    end
    movegui(FigH, 'center')
end