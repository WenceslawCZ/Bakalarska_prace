clear all
close all


figure('Renderer', 'painters', 'Position', [300 50 890 700])
set(gca,'FontSize',14)
%set(gca,'fontname','Microsoft Sans Serif')
cmap = colormap(lines(3));
hold on
%y3 = [0; y3]
%x2 = [90; x2]
for i = 1:1:3
    cesta = sprintf('uhel_pwm%d.txt',i)
    data = importdata(cesta);
    cesta = sprintf('%d. měření',i);
    scatter(data(:,1),data(:,2),'MarkerEdgeColor',[0.5 0.5 0.5],'MarkerFaceColor',cmap(i,:),'DisplayName',cesta);
    p = polyfit(data(:,1),data(:,2),1);
    x1 = linspace(0,100,1000);
    y1 = polyval(p,x1);
    plot(x1,y1, 'Color', cmap(i,:),'LineStyle',':','DisplayName','Proložení bodů polynomem 1.řádu', 'LineWidth',1)

end
data = importdata('uhel_pwm2.txt');
data2 = importdata('uhel_pwm3.txt');
data2 = (data(4:end,:)+data2)/2;
p = polyfit(data2(:,1),data2(:,2),1);
x1 = linspace(0,100,1000);
y1 = polyval(p,x1);
plot(x1,y1, 'Color', 'black','LineStyle','--','DisplayName','Proložení bodů polynomem 1.řádu', 'LineWidth',1,'HandleVisibility','off')


lgd = legend;
lgd.Location = 'northwest';
lgd.Orientation =  'horizontal';

lgd.NumColumns = 2;
title("Závislost úhlu natočení na velikosti PWM střídy");
xlabel('PWM střída   [%]')
ylabel('Úhel natočení od osy d   [°]');

hold off