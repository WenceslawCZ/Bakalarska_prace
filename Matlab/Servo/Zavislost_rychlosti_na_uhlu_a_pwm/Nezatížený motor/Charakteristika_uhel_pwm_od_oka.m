x = [0 10 20 30 40 50 60 70 80 90 100];
y1 = [90 90 90 91 92 98 102 103 109 116 123];
y2 = [90 91 92 94 96 100 104 109 113 119 125];

hold on
scatter(x, y1);
scatter(x, y2);


p = polyfit(x,y1,2);
x1 = linspace(0,100,1000);
y1 = polyval(p,x1);
plot(x1,y1, 'Color', cmap(11,:),'LineStyle',':','HandleVisibility','off', 'LineWidth',1)


p = polyfit(x,y2,2);
x1 = linspace(0,100,1000);
y1 = polyval(p,x1);
p(3) = 90;
plot(x1,y1, 'Color', cmap(11,:),'LineStyle',':','HandleVisibility','off', 'LineWidth',1)


hold off