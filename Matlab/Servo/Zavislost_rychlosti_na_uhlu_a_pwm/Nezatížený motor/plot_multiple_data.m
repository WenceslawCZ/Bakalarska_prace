clear all
close all

figure('Renderer', 'painters', 'Position', [300 50 890 700])
set(gca,'FontSize',14)
hold on
grid on

cmap = colormap(turbo(11));

x2 = zeros(10,1);
y2 = zeros(10,1);
y3 = zeros(10,1);

for i = 50:50:500
    cesta = sprintf('%d.txt',i)
    data = importdata(cesta);
    p = polyfit(data(:,1),data(:,2),7);
    x1 = linspace(data(1,1),data(end,1),1000);
    y1 = polyval(p,x1);
    nazev = sprintf('%d%% střída',0.2*i)
    
    plot(x1,y1,'DisplayName',nazev, 'Color', cmap(i/50,:),'LineWidth',1.5)
    index = find(y1==max(y1))
    scatter(x1(index(1)),y1(index(1)),'MarkerEdgeColor',[0.5 0.5 0.5],'MarkerFaceColor',cmap(i/50,:),'HandleVisibility','off');
    x2(i/50) = x1(index(1));
    y2(i/50) = y1(index(1));
    y3(i/50) = 0.2*i;
    %plot(data(:,1),data(:,2))
    %index = find(data(:,2)==max(data(:,2)))
    %scatter(data(index(1),1), max(data(:,2)),'red','filled');
    %scatter(data(21,1), data(21,2),'green','filled');
end

p = polyfit(x2,y2,2);
x1 = linspace(x2(1),x2(end),1000);
y1 = polyval(p,x1);
plot(x1,y1, 'Color', cmap(11,:),'LineStyle',':','HandleVisibility','off', 'LineWidth',1)

lgd = legend;
lgd.NumColumns = 2;
lgd.Location = 'northwest';
title("Závislost rychlosti na úhlu natočení pro jednotlivé PWM střídy");
xlabel('Úhel natočení od osy d   [°]');
ylabel('RPM   [1/min]')

hold off

% figure();
% hold on
% %y3 = [0; y3]
% %x2 = [90; x2]
% for i = 50:50:500
%     scatter(y3,x2,'MarkerEdgeColor',[0.5 0.5 0.5],'MarkerFaceColor',cmap(i/50,:),'HandleVisibility','off');
% end
% 
% writematrix([y3, x2],'uhel_pwm.txt');
% p = polyfit(y3,x2,1);
% x1 = linspace(0,100,1000);
% y1 = polyval(p,x1);
% plot(x1,y1, 'Color', cmap(11,:),'LineStyle',':','DisplayName','Proložení bodů polynomem 1.řádu', 'LineWidth',1)
% 
% lgd = legend;
% lgd.Location = 'northwest';
% title("Závislost úhlu natočení na velikosti PWM střídy");
% xlabel('PWM střída   [%]')
% ylabel('Úhel natočení od první fáze   [°]');
% 
% hold off