clear all
close all
data = importdata('50.txt');

hold on
plot(data(:,1),data(:,2))
ylim([min(data(:,2))-30,max(data(:,2))+30]);
index = find(data(:,2)==max(data(:,2)))
line([data(1,1),data(end,1)],[max(data(:,2)),max(data(:,2))],'Color','red','LineStyle','--'); 
line([data(index(1),1),data(index(1),1)],[min(data(:,2))-30,max(data(:,2))+30],'Color','red','LineStyle','--'); 
scatter(data(index(1),1), max(data(:,2)),'red','filled');

line([data(1,1),data(end,1)],[data(21,2),data(21,2)],'Color','green','LineStyle','--'); 
line([data(21,1),data(21,1)],[min(data(:,2))-30,max(data(:,2))+30],'Color','green','LineStyle','--'); 
scatter(data(21,1), data(21,2),'green','filled');
hold off
title("PWM střída 82%");
xlabel('Úhel natočení od první fáze   [°]');
ylabel('RPM   [1/min]')