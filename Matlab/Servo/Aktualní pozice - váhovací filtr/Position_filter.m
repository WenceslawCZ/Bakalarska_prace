clear all
close all
data = importdata('60_offset.txt');

figure('Renderer', 'painters', 'Position', [300 50 890 700])
hold on
grid on

data(:,4) = data(:,4) - data(1,4)*ones(length(data(:,4)),1);
data(:,4) = data(:,4)*(30.5*10^-6);
data(:,1) = (360/16383)*data(:,1);
data(:,2) = (360/16383)*data(:,2);
data(:,3) = (360/16383)*data(:,3);

plot(data(:,4), data(:,1),'LineWidth',1.2);
plot(data(:,4), data(:,2),'LineWidth',1);
plot(data(:,4), data(:,3),'LineWidth',1);

set(gca,'FontSize',16)
title("Měření aktuální polohy serva za pomocí váhového filtru");
xlabel('Čas  [s]');
ylabel('Absolutní pozice serva  [°]')
legend({'Úhel', 'Vypočítaná absolutní pozice serva', 'Absolutní pozice prvního enkodéru', 'Absolutní pozice druhého enkodéru'},'Location','Northwest');



hold off