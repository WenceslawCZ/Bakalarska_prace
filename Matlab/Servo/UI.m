close all
clear all

h_figure=figure(1);
clf
h_figure.CloseRequestFcn=@close_fcn;

global h_plot buffer1 buffer2 check_timer
buffer1=zeros(100,1); % buffer for drawing
buffer2=zeros(100,1); 
% gui elemetns
h_axes=axes('Position',[0.2 0.1 0.7 0.8],'XGrid','on','YGrid','on');
hold on
h_plot=plot(h_axes,buffer1);
angle_slider=uicontrol('Style','slider','Units','Normalized','Position',[0.08 0.1 0.05 0.8],'Min',-360,'Max',360,'value',0);
addlistener(angle_slider,'ContinuousValueChange',@slider_callback);

RPM_slider=uicontrol('Style','slider','Units','Normalized','Position',[0.08 0.1 0.05 0.8],'Min',0,'Max',20,'value',0);
addlistener(RPM_slider,'ContinuousValueChange',@slider_callback);


% Initialize USB Interface
USBDriver('callback_data_received',@data_received);
USBDriver('connect');
check_timer=timer('TimerFcn','USBDriver(''check'')','Period',0.05,'executionMode','fixedSpacing'); 
start(check_timer);

% Send data
USBDriver('send',int32(9756));


% Callback
function data_received(data)
    global buffer1 buffer2 h_plot
%     znaky = string(char(typecast(data,'uint8')'));
%     message = [znaky{:}];
%     
%     for i = 1:size(message,2)
%         if (message(i) == ';')
%             cropped_message = message(1:i-1)
%         end
%     end
%     splited_data = split(cropped_message);
    data
    buffer1=[buffer1(2:end);(360.0/16383.0)*double(data(2))];
    %buffer2=[buffer2(2:end);(360/16383)*data(2)];
    h_plot.YData=buffer1; %buffer2};
    drawnow
end

function slider_callback(obj,ev)
    USBDriver('send',int32(obj.Value*100));
end

% disconnects when figure is closed
function close_fcn(obj,ev)
    global check_timer
    %stop(check_timer)
    USBDriver('disconnect');
    delete(obj)
end