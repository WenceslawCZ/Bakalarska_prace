8 % Initialize USB Interface
USBDriver('callback_data_received',@data_received);
USBDriver('connect');
check_timer=timer('TimerFcn','USBDriver(''check'')','Period',0.05,'executionMode','fixedSpacing'); 
start(check_timer);

% Send data
USBDriver('send',uint32([91755 108 409]));

global soubor;
soubor = fopen( 'results.txt', 'wt' );
global offset;
offset = 0;
% Callback
function data_received(data)
    %global soubor;
    global soubor;
    global offset;
    znaky = string(char(typecast(data,'uint8')'));
    message = [znaky{:}];
    
    for i = 1:size(message,2)
        if (message(i) == ';')
            cropped_message = message(1:i-1)
        end
    end
    splited_data = split(cropped_message);
    
    if(str2double(splited_data(7)) ~= offset)
        cesta = sprintf('%d.txt',str2double(splited_data(7)))
        %cesta = splited_data(2) + ".txt"
        fclose(soubor);
        soubor = fopen(cesta, 'wt');
        offset = str2double(splited_data(7));
    end
    
    fprintf(soubor, '%f,%d,%d\n',str2double(splited_data(4))/150+str2double(splited_data(2)), str2double(splited_data(5)), str2double(splited_data(6)));
    
    %angle = (360/(2^14 -1)) * str2double(splited_data(3))
    %compass(cos(deg2rad(angle)),sin(deg2rad(angle)))
end
