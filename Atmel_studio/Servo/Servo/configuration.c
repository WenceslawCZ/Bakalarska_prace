﻿/*
 * configuration.c
 *
 * Created: 21-May-21 21:38:40
 *  Author: Vaclav
 */ 

#include "configuration.h"
#include "sam.h"
#include "pinMacro.h"
#include "pins.h"

 void configure_external_oscillator() {
	 MCLK->APBAMASK.bit.OSCCTRL_ = 1;

	 // Enable external oscillator
	 OSCCTRL->XOSCCTRL[0].bit.STARTUP = 0xA;
	 OSCCTRL->XOSCCTRL[0].bit.ENALC = 1;
	 OSCCTRL->XOSCCTRL[0].bit.IMULT = 6;
	 OSCCTRL->XOSCCTRL[0].bit.IPTAT = 3;
	 OSCCTRL->XOSCCTRL[0].bit.LOWBUFGAIN = 1;
	 OSCCTRL->XOSCCTRL[0].bit.ONDEMAND = 0;
	 OSCCTRL->XOSCCTRL[0].bit.XTALEN = 1;
	 OSCCTRL->XOSCCTRL[0].bit.ENABLE = 1;
	 
	 OSCCTRL->INTENSET.bit.XOSCRDY0 = 1;
	 while(!OSCCTRL->STATUS.bit.XOSCRDY0);

	 // Using external oscillator as a source for GLCK1 (32MHz)
	 GCLK_GENCTRL_Type genctrl;
	 genctrl.bit.DIV=1;
	 genctrl.bit.DIVSEL=0; // The generic clock generator equals the clock source divided by DIV.
	 genctrl.bit.RUNSTDBY=1; //The generic clock generator is kept running during standby mode.
	 genctrl.bit.OE=0; // The generic clock generator is not output
	 genctrl.bit.IDC=1; // The generic clock generator duty cycle is 50/50
	 genctrl.bit.GENEN=1; // The generic clock generator is enabled.
	 genctrl.bit.SRC=GCLK_GENCTRL_SRC_XOSC0_Val;
	 while(GCLK->SYNCBUSY.bit.GENCTRL1);
	 GCLK->GENCTRL[1] = genctrl;
	 
 }

 void configure_microcontroller_clock() {

	 GCLK_PCHCTRL_Type pchctrl;
	 pchctrl.bit.CHEN = 1;
	 pchctrl.bit.GEN = 0x1;
	 GCLK->PCHCTRL[1] = pchctrl;
	 
	 OSCCTRL->Dpll[0].DPLLRATIO.bit.LDR = 119;
	 OSCCTRL->Dpll[0].DPLLCTRLB.bit.REFCLK = OSCCTRL_DPLLCTRLB_REFCLK_XOSC0_Val;
	 OSCCTRL->Dpll[0].DPLLCTRLA.bit.ONDEMAND = 0;
	 OSCCTRL->Dpll[0].DPLLCTRLB.bit.DIV = 15;
	 OSCCTRL->Dpll[0].DPLLCTRLA.bit.ENABLE = 1;
	 
	 while(!OSCCTRL->Dpll[0].DPLLSTATUS.bit.LOCK);
	 
	 // Using DPLL0 as a source for uC clock - GLCK0 (120MHz)
	 GCLK_GENCTRL_Type genctrl;
	 genctrl.bit.DIV=1;
	 genctrl.bit.DIVSEL=0; // The generic clock generator equals the clock source divided by GENDIV.DIV.
	 genctrl.bit.RUNSTDBY=1; //The generic clock generator is kept running during standby mode.
	 genctrl.bit.OE=0; // The generic clock generator is not output
	 genctrl.bit.IDC=1; // The generic clock generator duty cycle is 50/50
	 genctrl.bit.GENEN=1; // The generic clock generator is enabled.
	 genctrl.bit.SRC = GCLK_GENCTRL_SRC_DPLL0_Val;
	 while(GCLK->SYNCBUSY.bit.GENCTRL0);
	 GCLK->GENCTRL[0] = genctrl;
 }

 void configure_DRV8832() {
	 //PORT_MUX(DRV8332_TEST, 6);
	 //DIR_OUT(DRV8332_TEST);
	 
	 PORT_MUX(DRV8332_PWM_A, 6);
	 PORT_MUX(DRV8332_PWM_B, 6);
	 PORT_MUX(DRV8332_PWM_C, 6);
	 PORT_MUX(DRV8332_RESET_A, 6);
	 PORT_MUX(DRV8332_RESET_B, 6);
	 PORT_MUX(DRV8332_RESET_C, 6);
	 DIR_OUT(DRV8332_PWM_A);
	 DIR_OUT(DRV8332_PWM_B);
	 DIR_OUT(DRV8332_PWM_C);
	 DIR_OUT(DRV8332_RESET_A);
	 DIR_OUT(DRV8332_RESET_B);
	 DIR_OUT(DRV8332_RESET_C);
	 DIR_IN(DRV8332_OTW);
	 DIR_IN(DRV8332_FAULT);
	 RESET(DRV8332_PWM_A);
	 RESET(DRV8332_RESET_A);
	 RESET(DRV8332_PWM_B);
	 RESET(DRV8332_PWM_C);
	 RESET(DRV8332_RESET_B);
	 RESET(DRV8332_RESET_C);
	 
	 
	 GCLK_PCHCTRL_Type pchctrl;
	 pchctrl.bit.CHEN = 1;
	 pchctrl.bit.GEN = 0x1;
	 GCLK->PCHCTRL[2] = pchctrl;
	 
	 OSCCTRL->Dpll[1].DPLLRATIO.bit.LDR = 191;
	 OSCCTRL->Dpll[1].DPLLCTRLB.bit.REFCLK = OSCCTRL_DPLLCTRLB_REFCLK_XOSC0_Val;
	 OSCCTRL->Dpll[1].DPLLCTRLA.bit.ONDEMAND = 0;
	 OSCCTRL->Dpll[1].DPLLCTRLB.bit.DIV = 15;
	 OSCCTRL->Dpll[1].DPLLCTRLA.bit.ENABLE = 1;
	 
	 while(!OSCCTRL->Dpll[1].DPLLSTATUS.bit.LOCK);
	 
	 // Using DPLL1 as a source for timer controlling DRV8332 - GLCK2 (120MHz)
	 GCLK_GENCTRL_Type genctrl;
	 genctrl.bit.DIV=1;
	 genctrl.bit.DIVSEL=0; // The generic clock generator equals the clock source divided by GENDIV.DIV.
	 genctrl.bit.RUNSTDBY=1; //The generic clock generator is kept running during standby mode.
	 genctrl.bit.OE=0; // The generic clock generator is not output
	 genctrl.bit.IDC=1; // The generic clock generator duty cycle is 50/50
	 genctrl.bit.GENEN=1; // The generic clock generator is enabled.
	 genctrl.bit.SRC = GCLK_GENCTRL_SRC_DPLL1_Val;
	 while(GCLK->SYNCBUSY.bit.GENCTRL2);
	 GCLK->GENCTRL[2] = genctrl;
	 
	 //GCLK_PCHCTRL_Type pchctrl;
	 pchctrl.bit.CHEN = 1;
	 pchctrl.bit.GEN = 0x2;
	 GCLK->PCHCTRL[25] = pchctrl; // GCLK_TCC0, GCLK_TCC1
	 
	 MCLK->APBBMASK.bit.TCC0_ = 1;
	 TCC0->CTRLA.bit.RUNSTDBY = 1;
	 TCC0->WAVE.bit.WAVEGEN = 0x2;
	 TCC0->CC[0].bit.CC = 0;				//RESET_B
	 TCC0->WAVE.bit.POL0 = 1;
	 TCC0->CC[1].bit.CC = 0;				//PWM_B
	 TCC0->CC[2].bit.CC = 0;				//RESET_A
	 TCC0->WAVE.bit.POL2 = 1;
	 TCC0->CC[3].bit.CC = 0;				//PWM_A
	 TCC0->CC[4].bit.CC = 0;				//PWM_C
	 TCC0->CC[5].bit.CC = 0;				//RESET_C
	 TCC0->WAVE.bit.POL5 = 1;
	 TCC0->PER.bit.PER = 999;
	 TCC0->DBGCTRL.bit.DBGRUN = 1;
	 TCC0->CTRLA.bit.ENABLE = 1;
	 while(!TCC0->SYNCBUSY.bit.ENABLE);
 }

 void configure_timer2() {
	 PORT_MUX(BLUE, 5);
	 DIR_OUT(BLUE);
	 
	 GCLK_PCHCTRL_Type pchctrl;
	 pchctrl.bit.CHEN = 1;
	 pchctrl.bit.GEN = 0x1;
	 GCLK->PCHCTRL[29] = pchctrl;
	 
	 MCLK->APBCMASK.bit.TCC2_ = 1;
	 TCC2->CTRLA.bit.RUNSTDBY = 1;
	 TCC2->WAVE.bit.WAVEGEN = 0x2;
	 TCC2->DRVCTRL.bit.INVEN2 = 1;
	 TCC2->WAVE.bit.POL2 = 1;
	 TCC2->WAVE.bit.CICCEN2 = 1;
	 TCC2->CC[2].bit.CC = 0;
	 TCC2->PER.bit.PER = 65535;
	 TCC2->DBGCTRL.bit.DBGRUN = 1;
	 TCC2->CTRLA.bit.ENABLE = 1;
	 while(!TCC2->SYNCBUSY.bit.ENABLE);
 }

 void configure_timer4() {
	 PORT_MUX(RED, 5);
	 DIR_OUT(RED);
	 PORT_MUX(GREEN, 5);
	 DIR_OUT(GREEN);
	 
	 GCLK_PCHCTRL_Type pchctrl;
	 pchctrl.bit.CHEN = 1;
	 pchctrl.bit.GEN = 0x1;
	 GCLK->PCHCTRL[38] = pchctrl;
	 
	 MCLK->APBDMASK.bit.TCC4_ = 1;
	 TCC4->CTRLA.bit.RUNSTDBY = 1;
	 TCC4->WAVE.bit.WAVEGEN = 0x2;
	 TCC4->DRVCTRL.bit.INVEN0 = 0;
	 TCC4->DRVCTRL.bit.INVEN1 = 1;
	 TCC4->WAVE.bit.POL0 = 1;
	 TCC4->WAVE.bit.POL1 = 1;
	 TCC4->WAVE.bit.CICCEN0 = 1;
	 TCC4->WAVE.bit.CICCEN1 = 1;
	 TCC4->CC[0].bit.CC = 0;
	 TCC4->CC[1].bit.CC = 0;
	 TCC4->PER.bit.PER = 65535;
	 TCC4->DBGCTRL.bit.DBGRUN = 1;
	 TCC4->CTRLA.bit.ENABLE = 1;
	 while(!TCC4->SYNCBUSY.bit.ENABLE);
 }


 void configure_SPI_communication() {
	 DIR_OUT(AS5048A_CS1);
	 DIR_OUT(AS5048A_CS2);
	 DIR_OUT(AS5048A_SCK);
	 DIR_IN(AS5048A_MISO);
	 DIR_OUT(AS5048A_MOSI);
	 
	 SET(AS5048A_CS1);
	 SET(AS5048A_CS2);
	 
	 PORT_MUX(AS5048A_SCK,2);  // SERCOM4/PAD[1]
	 PORT_MUX(AS5048A_MISO,2); // SERCOM4/PAD[2]
	 PORT_MUX(AS5048A_MOSI,2); // SERCOM4/PAD[3]
	 
	 // Configure SERCOM4 generic clock (GCLK_SERCOM4_CORE)
	 GCLK_PCHCTRL_Type pchctrl;
	 pchctrl.bit.CHEN = 1;
	 pchctrl.bit.GEN = 0x4;
	 GCLK->PCHCTRL[34] = pchctrl;
	 
	 MCLK->APBDMASK.bit.SERCOM4_ = 1;
	 
	 SERCOM4->SPI.CTRLA.bit.DORD = 0; // MSB is transferred first.
	 SERCOM4->SPI.CTRLA.bit.CPOL = 0; // Mode 1, Leading Edge - Rising (change), Trailing Edge - Falling (sample)
	 SERCOM4->SPI.CTRLA.bit.CPHA = 1; // The data is sampled on a trailing SCK edge and changed on a leading SCK edge.
	 //SERCOM4->SPI.CTRLA.bit.FORM=0;
	 SERCOM4->SPI.CTRLA.bit.DIPO = 0x2;
	 SERCOM4->SPI.CTRLA.bit.DOPO = 0x2;
	 SERCOM4->SPI.CTRLA.bit.RUNSTDBY = 1;
	 // Not sure SERCOM4->SPI.CTRLA.bit.IBON=1;-> buffer overflow
	 // Not sure SERCOM4->SPI.BAUD.bit.BAUD = 65206;
	 SERCOM4->SPI.CTRLA.bit.MODE = 0x3; //SPI master operation
	 SERCOM4->SPI.CTRLB.bit.RXEN=1; // The receiver is enabled or it will be enabled when SPI is enabled.
	 SERCOM4->SPI.CTRLB.bit.MSSEN=0; //nCS line can be located at any general purpose I/O pin, must be controlled by software
	 
	 //SERCOM4->SPI.CTRLC.bit.DATA32B = 1;
	 
	 //SERCOM4->SPI.LENGTH.bit.LENEN = 1;
	 //SERCOM4->SPI.LENGTH.bit.LEN = 16;

	 SERCOM4->SPI.CTRLA.bit.ENABLE=1;
	 while(SERCOM4->SPI.SYNCBUSY.bit.ENABLE);
 }

 void configure_USB_clock() {
	 GCLK_GENCTRL_Type genctrl;
	 genctrl.bit.DIV=4;
	 genctrl.bit.DIVSEL=0; // The generic clock generator equals the clock source divided by GENDIV.DIV.
	 genctrl.bit.RUNSTDBY=1; //The generic clock generator is kept running during standby mode.
	 genctrl.bit.OE=0; // The generic clock generator is not output
	 genctrl.bit.IDC=1; // The generic clock generator duty cycle is 50/50
	 genctrl.bit.GENEN=1; // The generic clock generator is enabled.
	 genctrl.bit.SRC = GCLK_GENCTRL_SRC_DPLL1_Val;
	 while(GCLK->SYNCBUSY.bit.GENCTRL3);
	 GCLK->GENCTRL[3] = genctrl;
 }

 void configure_UART() {
	 PORT_MUX(UART0_TX, 2);
	 PORT_MUX(UART1_TX, 3);
	 PORT_MUX(UART0_RX, 2);
	 PORT_MUX(UART1_RX, 3);
	 DIR_OUT(UART0_TX);
	 DIR_OUT(UART1_TX);
	 DIR_IN(UART0_RX);
	 DIR_IN(UART1_RX);
	 DIR_OUT(UART0_DIR);
	 DIR_OUT(UART1_DIR);
	 SET(UART0_DIR);
	 SET(UART1_DIR);

	 // Use external oscillator as a source for UART - GLCK4 (16MHz)
	 GCLK_GENCTRL_Type genctrl;
	 genctrl.bit.DIV=2;
	 genctrl.bit.DIVSEL=0; // The generic clock generator equals the clock source divided by DIV.
	 genctrl.bit.RUNSTDBY=1; //The generic clock generator is kept running during standby mode.
	 genctrl.bit.OE=0; // The generic clock generator is not output
	 genctrl.bit.IDC=1; // The generic clock generator duty cycle is 50/50
	 genctrl.bit.GENEN=1; // The generic clock generator is enabled.
	 genctrl.bit.SRC=GCLK_GENCTRL_SRC_XOSC0_Val;
	 while(GCLK->SYNCBUSY.bit.GENCTRL4);
	 GCLK->GENCTRL[4] = genctrl;

	 
	 GCLK_PCHCTRL_Type pchctrl;
	 pchctrl.bit.CHEN = 1;
	 pchctrl.bit.GEN = 0x4;
	 GCLK->PCHCTRL[7] = pchctrl; //GCLK_SERCOM0_CORE
	 
	 MCLK->APBAMASK.bit.SERCOM0_ = 1;
	 
	 // Configure SERCOM2 generic clock (16 MHz)
	 SERCOM0->USART.CTRLA.bit.DORD = 1;	// MSB is transmitted first
	 // SERCOM0->USART.CTRLA.bit.CPOL = 0;   // Data sampling
	 SERCOM0->USART.CTRLA.bit.CMODE = 0;		// Asynchronous communication
	 SERCOM0->USART.CTRLA.bit.FORM = 0;		// USART frame without parity
	 SERCOM0->USART.CTRLA.bit.RXPO = 0x2;		// SERCOM0 PAD[2] is used for data reception
	 SERCOM0->USART.CTRLA.bit.TXPO = 0x0;		// SERCOM0 PAD[0] is used for data transmission
	 SERCOM0->USART.CTRLA.bit.SAMPR = 2;		// 8x over-sampling using arithmetic baud rate generation
	 SERCOM0->USART.CTRLA.bit.RUNSTDBY = 1;
	 SERCOM0->USART.CTRLA.bit.MODE = 1;		// USART with internal clock
	 SERCOM0->USART.CTRLB.bit.RXEN = 1;		// The receiver is enabled
	 SERCOM0->USART.CTRLB.bit.TXEN = 1;		// The transmitter is enabled
	 SERCOM0->USART.BAUD.bit.BAUD = 0;		// BAUD_value to get 1Mega BAUD_rate
	 SERCOM0->USART.INTENSET.bit.DRE = 1;
	 SERCOM0->USART.INTENSET.bit.TXC = 1;
	 SERCOM0->USART.CTRLA.bit.ENABLE = 1;
	 while(SERCOM0->USART.SYNCBUSY.bit.ENABLE);
	 
	 
	 pchctrl.bit.CHEN = 1;
	 pchctrl.bit.GEN = 0x4;
	 GCLK->PCHCTRL[23] = pchctrl; //GCLK_SERCOM2_CORE
	 
	 MCLK->APBBMASK.bit.SERCOM2_ = 1;
	 
	 SERCOM2->USART.CTRLA.bit.DORD = 1;	// MSB is transmitted first
	 // SERCOM2->USART.CTRLA.bit.CPOL = 0;   // Data sampling
	 SERCOM2->USART.CTRLA.bit.CMODE = 0;		// Asynchronous communication
	 SERCOM2->USART.CTRLA.bit.FORM = 0;		// USART frame without parity
	 SERCOM2->USART.CTRLA.bit.RXPO = 0x3;		// SERCOM2 PAD[2] is used for data reception
	 SERCOM2->USART.CTRLA.bit.TXPO = 0x0;		// SERCOM2 PAD[0] is used for data transmission
	 SERCOM2->USART.CTRLA.bit.SAMPR = 2;		// 8x over-sampling using arithmetic baud rate generation
	 SERCOM2->USART.CTRLA.bit.RUNSTDBY = 1;
	 SERCOM2->USART.CTRLA.bit.MODE = 1;		// USART with internal clock
	 SERCOM2->USART.CTRLB.bit.RXEN = 1;		// The receiver is enabled
	 SERCOM2->USART.CTRLB.bit.TXEN = 1;		// The transmitter is enabled
	 SERCOM2->USART.BAUD.bit.BAUD = 60503;	// BAUD_value to get 9600 BAUD_rate
	 SERCOM2->USART.INTENSET.bit.DRE = 1;
	 SERCOM2->USART.INTENSET.bit.TXC = 1;
	 SERCOM2->USART.CTRLA.bit.ENABLE = 1;
	 while(SERCOM2->USART.SYNCBUSY.bit.ENABLE);
 }


 void configure_RTC() {
	 MCLK->APBAMASK.bit.OSC32KCTRL_ = 1;

	 OSC32KCTRL->OSCULP32K.bit.EN32K = 1;
	 OSC32KCTRL->RTCCTRL.bit.RTCSEL = OSC32KCTRL_RTCCTRL_RTCSEL_ULP32K_Val;
	 
	 MCLK->APBAMASK.bit.RTC_ = 1;

	 RTC->MODE0.CTRLA.bit.COUNTSYNC = 1;
	 RTC->MODE0.CTRLA.bit.PRESCALER = 0;
	 RTC->MODE0.CTRLA.bit.MODE = 0;

	 RTC->MODE0.CTRLA.bit.ENABLE = 1;
	 while(RTC->MODE0.SYNCBUSY.bit.ENABLE);
 }
