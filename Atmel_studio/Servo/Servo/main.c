/*
 * Servo.c
 *
 * Created: 18.04.2021 17:24:10
 * Author : vaclav
 */ 


#include "sam.h"
#include <math.h>
#include <stdbool.h>
#define _OPEN_SYS_ITOA_EXT
#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include "../USB_library/USB.h"
#include "pinMacro.h"
#include "AS5048A_macro.h"
#include "pins.h"
#include "functions.h"
#include "configuration.h"

uint8_t rx_data_buffer[20]; // buffer for data reception
uint8_t tmp_buffer[20]; // temporary buffer

volatile bool start = false;

struct message {
	int32_t angle;
	int32_t RPM;
	int32_t P;
	int32_t I;
	int32_t D;
} received_message;

struct values {
	int32_t angle;
	float RPM;
	float P;
	float I;
	float D;
} received_values;

struct S_message {
	int32_t reference;
	int32_t actual_position;
} send_message;

void usb_data_received(uint8_t status, uint16_t length) {
	//memcpy(tmp_buffer,rx_data_buffer,length); // copies data to temporary buffer
	//usb_send_bulk_data(tmp_buffer,length,0); // resends data
	//usb_receive_bulk_data(); // restarts data reception
	//memcpy(&received_message, tmp_buffer, length);
	memcpy(&received_message, rx_data_buffer, length);
	received_values.angle = (int32_t)(0.4550833*(float)received_message.angle); //16383/36000 -> getting float number with 2 decimal points
	received_values.RPM = (float)(received_message.RPM)/100.0;
	received_values.P = (float)(received_message.P)/100.0;
	received_values.I = (float)(received_message.I)/100.0;
	received_values.D = (float)(received_message.D)/100.0;
	start = true;
	usb_receive_bulk_data(); // restarts data reception
}

void set_usb_config(uint8_t index) {
	usb_setup_rx_bulk_buffer((uint8_t*)rx_data_buffer,sizeof(rx_data_buffer),usb_data_received); // sets reception buffer and reception callback
	usb_receive_bulk_data(); // starts data reception
}

#define		calibration_cycles		40

int main(void)
{
    /* Initialize the SAM system */
    SystemInit();
    configure_external_oscillator();
	configure_microcontroller_clock();
	configure_DRV8832();
	configure_timer2();
	configure_timer4();
	configure_USB_clock();
	configure_UART();
	configure_SPI_communication();
	configure_RTC();

	usb_register_set_config_callback(set_usb_config);
	usb_init(3); // the parameter of this function sets the GCKL number from which the USB peripheral is taking its clock (in this case it is GCLK0). This clock needs to be 48MHz
	
	
	struct Encoders encoder1; 
	encoder_setup(&encoder1, 0); 

	struct Encoders encoder2;
	encoder_setup(&encoder2, 1); 
	
	struct actual_position position;
	position = setup_position((int32_t)(encoder1.actual_position/150), encoder2.actual_position);
	
	
	long index = 0;
	int pwm = 0;
	float voltage_faze_1 = 0, voltage_faze_2 = 0, voltage_faze_3 = 0;
	long encoder_to_motor_phase_offset = 0;

	int8_t direction = 1;
	HSV(240, 1, 1);
	for(int i = 0; i < calibration_cycles; i++) {
		pwm = 400;
		for (int angle = 0; angle < 500; angle++) {
			voltage_faze_1 = pwm+pwm*sin(data_to_rad(direction*angle));
			voltage_faze_2 = pwm+pwm*sin(data_to_rad(direction*angle)-PHAZE_SHIFT);
			voltage_faze_3 = pwm+pwm*sin(data_to_rad(direction*angle)+PHAZE_SHIFT);
			power(PWM_A, (int)voltage_faze_1);
			power(PWM_B, (int)voltage_faze_2);
			power(PWM_C, (int)voltage_faze_3);
		}
		pwm = 500;
		voltage_faze_1 = pwm+pwm*sin(0);
		voltage_faze_2 = pwm+pwm*sin(-PHAZE_SHIFT);
		voltage_faze_3 = pwm+pwm*sin(+PHAZE_SHIFT);
		power(PWM_A, (int)voltage_faze_1);
		power(PWM_B, (int)voltage_faze_2);
		power(PWM_C, (int)voltage_faze_3);
		delay_ms(40);
		encoder_update(&encoder1);
		encoder_to_motor_phase_offset += encoder1.actual_angle;
		encoder_update(&encoder2);
		position.encoder1_position = (int32_t)(encoder1.actual_position/150);
		position.encoder2_position = encoder2.actual_position;
		get_position(&position);

		if (direction == 1) {
			direction = -1;
		}
		else{
			direction = 1;
		}
	}
	
	encoder_to_motor_phase_offset /= calibration_cycles;
	power(PWM_A, 0);
	power(PWM_B, 0);
	power(PWM_C, 0);

	HSV(120, 1, 1);
	while (!start) {
		encoder_update(&encoder1);
		encoder_update(&encoder2);
		position.encoder1_position = (int32_t)((encoder1.actual_position)/150);
		position.encoder2_position = encoder2.actual_position;
		get_position(&position);
		//send_message.actual_position = position.actual_position; //reference_position;//
		//send_message.reference = received_values.angle;
		//usb_send_bulk_data(&send_message,8,0);
		//usb_receive_bulk_data(); // restarts data reception
	};

	//float RPM = 10;
	
	float reference_position = position.actual_position;
	int32_t error = 0, previous_error = 0, error_sum = 0;

	while (1)
	{	
		encoder_update(&encoder1);		
		encoder_update(&encoder2);

		position.encoder1_position = (int32_t)((encoder1.actual_position)/150);
		position.encoder2_position = encoder2.actual_position;
		get_position(&position);

		if (reference_position < received_values.angle) {
			reference_position += (received_values.RPM/60)*30.5*0.000001*(encoder2.actual_time-encoder2.previous_time)*16383;
			if (reference_position > received_values.angle) {
				reference_position = received_values.angle;
			}
		}
		else if (reference_position > received_values.angle){
			reference_position -= (received_values.RPM/60)*30.5*0.000001*(encoder2.actual_time-encoder2.previous_time)*16383;
			if (reference_position < received_values.angle) {
				reference_position = received_values.angle;
			}
		}
		
		error = reference_position-position.actual_position;
		error_sum = constrain(error_sum+error, -3000, 3000);
		pwm = received_values.P*error + received_values.I*error_sum*30.5*0.000001*(encoder2.actual_time-encoder2.previous_time+0.1) + received_values.D*(error - previous_error)/(30.5*0.000001*(encoder2.actual_time-encoder2.previous_time+0.1));

		previous_error = error;

		if (pwm >= 0) {
			direction = 1;
		}
		else {
			direction = -1;
			pwm = -pwm;
		}
		if(pwm > 500) {
			pwm = 500;
		}

		if(abs(error) < 2) {
			pwm = 0;
		}


		voltage_faze_1 = pwm+pwm*sin(-data_to_rad(encoder_to_motor_phase_offset*number_of_pole_pairs)+direction*deg_to_rad(0.04422*2*pwm + 89.0068)+data_to_rad(encoder1.actual_angle*number_of_pole_pairs));
		voltage_faze_2 = pwm+pwm*sin(-data_to_rad(encoder_to_motor_phase_offset*number_of_pole_pairs)+direction*deg_to_rad(0.04422*2*pwm + 89.0068)+data_to_rad(encoder1.actual_angle*number_of_pole_pairs) - PHAZE_SHIFT);
		voltage_faze_3 = pwm+pwm*sin(-data_to_rad(encoder_to_motor_phase_offset*number_of_pole_pairs)+direction*deg_to_rad(0.04422*2*pwm + 89.0068)+data_to_rad(encoder1.actual_angle*number_of_pole_pairs) + PHAZE_SHIFT);

		power(PWM_A, (int)voltage_faze_1);
		power(PWM_B, (int)voltage_faze_2);
		power(PWM_C, (int)voltage_faze_3);
		
		index++;

		if (index > 200) {
			index = 0;

			if(encoder1.reading_error || encoder2.reading_error) {
				HSV(0, 1, 1);
				encoder1.reading_error = false;
				encoder2.reading_error = false;
				delay_ms(20);
				HSV(0, 1, 0);
			}

			send_message.actual_position = position.actual_position; //reference_position;//
			send_message.reference = received_values.angle;
			usb_send_bulk_data(&send_message,8,0);
			//usb_receive_bulk_data(); // restarts data reception
		}
		
		HSV(encoder2.actual_angle/45.6, 1, 0.01);
	}
	while(1){
		HSV(0, 1, 1);
		power(PWM_A, 0);
		power(PWM_B, 0);
		power(PWM_C, 0);
		delay_ms(100);
		HSV(0, 1, 0);
		delay_ms(100);
	};
}
