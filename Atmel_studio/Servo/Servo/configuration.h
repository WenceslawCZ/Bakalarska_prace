/*
 * configuration.h
 *
 * Created: 05.08.2020 17:33:10
 *  Author: vaclav
 */ 


#ifndef CONFIGURATION_H_
#define CONFIGURATION_H_

void configure_external_oscillator();
void configure_microcontroller_clock();
void configure_DRV8832();
void configure_timer2();
void configure_timer4();
void configure_SPI_communication();
void configure_USB_clock();
void configure_UART();
void configure_RTC();

#endif /* CONFIGURATION_H_ */