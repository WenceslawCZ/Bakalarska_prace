/*
 * pins.h
 *
 * Created: 06.03.2020 12:46:17
 *  Author: V�clav ��p + Bortel
 */ 

#ifndef _PINS_H_
#define _PINS_H_

// pins for led
#define RED				PB,30
#define GREEN			PB,31
#define BLUE			PB,02


// pins for DRV8332
#define		DRV8332_PWM_A		PA,23
#define		DRV8332_PWM_B		PA,21
#define		DRV8332_PWM_C		PA,16
#define		DRV8332_RESET_A		PA,22
#define		DRV8332_RESET_B		PA,20
#define		DRV8332_RESET_C		PA,17
#define		DRV8332_OTW			PB,17
#define		DRV8332_FAULT		PB,16

#define		DRV8332_TEST		PA,19


// pins for AS5048A
#define AS5048A_CS1		PB,11
#define AS5048A_CS2		PB,12
#define AS5048A_SCK		PB,13
#define AS5048A_MISO	PB,14
#define AS5048A_MOSI	PB,15

// pins for UART
#define UART0_TX		PA,8
#define UART0_RX		PA,10
#define UART0_DIR		PA,06
#define UART1_TX		PA,9
#define UART1_RX		PA,11
#define UART1_DIR		PA,07


// pins for PTC
#define		PTC_pin_1	PB,8


#endif
