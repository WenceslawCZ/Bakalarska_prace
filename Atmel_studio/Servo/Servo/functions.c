﻿/*
 * functions.c
 *
 * Created: 21-May-21 21:06:43
 *  Author: Václav
 */ 

#include "functions.h"
#include "sam.h"
#include "AS5048A_macro.h"
#include "pins.h"
#include "pinMacro.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "math.h"

 void delay_ms(float ms){
	long time = ms*8585;
	for (int n=0; n<time; n++) {asm volatile("nop");}
}

void delay_us(float us){
	long time = (int)us*8.49;
	for (int n=0; n<time; n++) {asm volatile("nop");}
}

void HSV(float hue, float saturation, float value) {
	int R = 0, G = 0, B = 0;
	float C = value*saturation;
	volatile float X = (C*(1.0-fabs(fmod((hue/60.0),2)-1.0)));
	float m = value - C;

	if (hue < 0) {
		R = 0;
		G = 0;
		B = 0;
	}
	else if (hue < 60) {
		R = (int)((C+m)*65535.0);
		G = (int)((X+m)*65535.0);
		B = 0;
	}
	else if (hue < 120) {
		R = (int)((X+m)*65535.0);
		G = (int)((C+m)*65535.0);
		B = 0;
	}
	else if (hue < 180) {
		R = 0;
		G = (int)((C+m)*65535.0);
		B = (int)((X+m)*65535.0);
	}
	else if (hue < 240) {
		R = 0;
		G = (int)((X+m)*65535.0);
		B = (int)((C+m)*65535.0);
	}
	else if (hue < 300) {
		R = (int)((X+m)*65535.0);
		G = 0;
		B = (int)((C+m)*65535.0);
	}
	else if (hue < 360) {
		R = (int)((C+m)*65535.0);
		G = 0;
		B = (int)((X+m)*65535.0);
	}
	else {
		R = 0;
		G = 0;
		B = 0;
	}
	
	TCC4->CC[0].bit.CC = R;
	while(!TCC4->SYNCBUSY.bit.CC0);
	TCC4->CC[1].bit.CC = G;
	while(!TCC4->SYNCBUSY.bit.CC1);
	TCC2->CC[2].bit.CC = B;
	while(!TCC2->SYNCBUSY.bit.CC2);
}

void RGB(float R, float G, float B) {
	if (R<0 || G<0 || B<0 || R>255 || G>255 || B>255) {
		R = 0;
		G = 0;
		B = 0;
	}
	TCC4->CC[0].bit.CC = (int)(R*257.0);
	while(!TCC4->SYNCBUSY.bit.CC0);
	TCC4->CC[1].bit.CC = (int)(G*257.0);
	while(!TCC4->SYNCBUSY.bit.CC1);
	TCC2->CC[2].bit.CC = (int)(B*257.0);
	while(!TCC2->SYNCBUSY.bit.CC2);
}

uint8_t SPI_transfer(uint8_t data) {
	while (!SERCOM4->SPI.INTFLAG.bit.DRE);
	SERCOM4->SPI.DATA.bit.DATA=data;
	while (!SERCOM4->SPI.INTFLAG.bit.RXC);
	return SERCOM4->SPI.DATA.bit.DATA;
}

uint16_t AS5048A_receive(uint16_t address, uint8_t id) {
	switch(id){
		case 0:
			RESET(AS5048A_CS1);
			break;
		case 1:
			RESET(AS5048A_CS2);
			break;
		default:
			break;
	}
	
	volatile uint8_t data1 = SPI_transfer(address >> 8);
	volatile uint8_t data2 = SPI_transfer(address & 0xFF);
	
	switch(id){
		case 0:
			SET(AS5048A_CS1);
			break;
		case 1:
			SET(AS5048A_CS2);
			break;
		default:
			break;
	}
	return data1<<8 | data2;
}

bool checkParity(uint16_t n)
{
    bool parity = false;
    while (n)
    {
        parity = !parity;
        n     = n & (n - 1);
    }    
    return parity;
}

bool checkFlag(uint16_t n)
{
    if (n & (1<<14)) {
		return true;	
	}
	else{
		return false;
	}
}


struct receive_UART_message UART_receive() {
	char UART_data[11] = "";
	char data;
	while(1) {
		data = SERCOM2->USART.DATA.bit.DATA;
		if(data=='#') {
			break;
		}
	}
	for(int i = 0; i<11;i++) {
		UART_data[i] = SERCOM2->USART.DATA.bit.DATA;
	}
	struct receive_UART_message message;
	memcpy(&message, UART_data, 11);
	return message;
}

#define		LEFT		0
#define		RIGHT		1
#define		ALL			2

void UART_send(uint8_t to_whom, struct send_UART_message *data) {
	if (to_whom == ALL || to_whom == LEFT) {
		RESET(UART0_DIR);
	}
	if (to_whom == ALL || to_whom == RIGHT) {
		RESET(UART1_DIR);
	}
	
	SERCOM2->USART.DATA.bit.DATA = data;

	if (to_whom == ALL || to_whom == LEFT) {
		SET(UART0_DIR);
	}
	if (to_whom == ALL || to_whom == RIGHT) {
		SET(UART1_DIR);
	}
}


#define		PWM_A		3
#define		PWM_B		1
#define		PWM_C		4
#define		RESET_A		2
#define		RESET_B		0
#define		RESET_C		5

void power(uint8_t index, int power) {
	if (power > 1000) {
		power = 1000;
	}
	else if (power < 0) {
		power = 0;
	}
	
	TCC0->CC[index].bit.CC = power;	
	
	/*switch(index) {
		case PWM_A:
			while(!TCC0->SYNCBUSY.bit.CC3);
			break;
		case PWM_B:
			while(!TCC0->SYNCBUSY.bit.CC1);
			break;
		case PWM_C:
			while(!TCC0->SYNCBUSY.bit.CC4);
			break;
		case RESET_A:
			while(!TCC0->SYNCBUSY.bit.CC2);
			break;
		case RESET_B:
			while(!TCC0->SYNCBUSY.bit.CC0);
			break;
		case RESET_C:
			while(!TCC0->SYNCBUSY.bit.CC5);
			break;	
		default:
			break;
	}*/
}

float data_to_rad(long data) {
	return 0.0003835186050894*data;    //((2*pi)/(2^14 -1))*data
}

float deg_to_rad(float angle) {
	if (angle > 90) {
		return 0.017453292519943*angle; //(2*pi)/(360)*angle
	}
	
	return 1.570796; // pi/2
}

float float_constrain(float value, float min, float max) {
	if (value > max) {
		value = max;
	}
	if (value < min) {
		value = min;
	}
	return value;
}

#define		PHAZE_SHIFT		2.094395102393195		//(2*pi)/3
#define		number_of_pole_pairs	2
#define		max_averaging_array_size		5



void encoder_setup(struct Encoders *encoder, uint8_t id) {
	encoder->id = id;

	volatile uint16_t data = AS5048A_receive(AS5048A_ANGLE, encoder->id);
	
	if (checkFlag(data) || checkParity(data)) {
		encoder->reading_error = true;
	}
	encoder->actual_angle = data & 0x3FFF;
	encoder->previous_angle = 0;

	while(RTC->MODE0.SYNCBUSY.bit.COUNT);
	encoder->actual_time = RTC->MODE0.COUNT.bit.COUNT;
	encoder->previous_time = 0;
	
	encoder->number_of_rotations = 0;

	encoder->actual_position = encoder->actual_angle;
}

void encoder_update(struct Encoders *encoder) {
	encoder->previous_angle = encoder->actual_angle;
	volatile uint16_t data = AS5048A_receive(AS5048A_ANGLE, encoder->id);
	uint16_t blink_red_light = 0;
	uint16_t turn_off_motor = 0;

	while (checkFlag(data) || checkParity(data)) {
		if (blink_red_light < 5000) {
			HSV(0, 1, 1);
		}
		else if (blink_red_light > 10000) {
			blink_red_light = 0;
			turn_off_motor++;
		}
		else {
			HSV(0, 1, 0);
		}

		if (turn_off_motor > 5) {
			power(PWM_A, 0);
			power(PWM_B, 0);
			power(PWM_C, 0);
		}
		encoder->reading_error = true;

		AS5048A_receive(AS5048A_CLEAR_ERROR_FLAG, encoder->id);
		data = AS5048A_receive(AS5048A_ANGLE, encoder->id);
		blink_red_light++;
	}

	encoder->actual_angle = data & 0x3FFF;
	
	if (encoder->id == 1) {
		//encoder->actual_angle += angle_compensation[(int)(0.19465298*encoder->actual_angle)];
	}

	encoder->previous_time = encoder->actual_time;
	while(RTC->MODE0.SYNCBUSY.bit.COUNT);
	encoder->actual_time = RTC->MODE0.COUNT.bit.COUNT;

	if (encoder->actual_angle - encoder->previous_angle < -12000) {
		encoder->number_of_rotations++;
	}
	else if (encoder->actual_angle - encoder->previous_angle > 12000) {
		encoder->number_of_rotations--;
	}
	
	encoder->actual_position = 16383*encoder->number_of_rotations + encoder->actual_angle;
}



struct actual_position setup_position(int32_t encoder1_position, int32_t encoder2_position) {
	struct actual_position position;
	position.encoder1_position = encoder1_position;
	position.encoder2_position = encoder2_position;
	position.weighting_coefficient = 0.0;

	if (position.encoder1_position > position.encoder2_position) {
		position.state = true;
	}
	else {
		position.state = false;
	}
	position.actual_position = (int32_t)(position.weighting_coefficient*position.encoder1_position + (1.0-position.weighting_coefficient)*position.encoder2_position);
	return position;
}

void get_position(struct actual_position *position) {
	position->encoder1_position += position->encoder_to_encoder_offset;
	if (position->encoder1_position > position->encoder2_position) {
		if (position->state == false) {
			position->state = true;
			position->weighting_coefficient += 0.2;
		}
		else {
			position->weighting_coefficient -= 0.002;
		}
		
	}
	else {
		if (position->state == true) {
			position->state = false;
			position->weighting_coefficient += 0.2;
		}
		else {
			position->weighting_coefficient -= 0.002;
		}
	}
	if (position->weighting_coefficient > 1) {
		position->weighting_coefficient = 1;
	}
	else if (position->weighting_coefficient < 0) {
		position->weighting_coefficient = 0;
		if (position->state == true) {
			position->encoder_to_encoder_offset -= abs(position->encoder1_position-position->encoder2_position)/2;
		}
		else {
			position->encoder_to_encoder_offset += abs(position->encoder1_position-position->encoder2_position)/2;
		}
	}
	position->actual_position = (int32_t)(position->weighting_coefficient*position->encoder1_position + (1.0-position->weighting_coefficient)*position->encoder2_position);
}

int32_t constrain(int32_t value, int32_t min, int32_t max) {
	if (value > max) {
		return max;
	}
	else if (value < min) {
		return min;
	}
	return value;
}