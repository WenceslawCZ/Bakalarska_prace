# Inteligentní servo - 3D modely

Soubory vytvořené v Solid Edge 2020.

## Informace:
* Všechny soubory se dají vytisknout na 3D tiskárně
* Tiskněte vždy poslední verzi modelu
* Výtisky jsou připravené na vstvu 0.2mm, infill 25%, bez podpor

## Potřebné součátky na sestavení
* 2x ložisko 6703ZZ
* 2x ložisko 6705ZZ
* 24x Ložiskové kuličky 5mm
* 1x [BLDC motor](https://www.omc-stepperonline.com/bldc-motor/24v-2000rpm-0062nm-13w-091a-round-%D1%8442x43mm-brushless-dc-motor-42blr43-24-01.html?mfp=200-rated-power-w%5B7%2C10%2C13%2C15%2C20%2C25%2C26%2C30%20W%2C46%2C50%20W%2C52.5%2C55%2C70%2C75%2C77.5%2C84%2C99%2C100%2C105%2C134%2C138%2C172%5D%2C201-rated-speed-rpm%5B2000%C2%B110%25%2C3000%C2%B110%25%5D)
* 1x [Gumový řemen 6mm](https://www.google.com/search?q=GT2+Closed+Loop+Timing+Rubber+Belt+280mm+Synchronous+Belt&tbm=isch&ved=2ahUKEwjC0fGeq-zvAhWM0YUKHc9VDV8Q2-cCegQIABAA&oq=GT2+Closed+Loop+Timing+Rubber+Belt+280mm+Synchronous+Belt&gs_lcp=CgNpbWcQA1C_kwFY67IBYL-0AWgDcAB4AYAB1QKIAZYLkgEIMTAuMS4xLjGYAQCgAQGqAQtnd3Mtd2l6LWltZ8ABAQ&sclient=img&ei=2MBtYMKFMoyjlwTPq7X4BQ&bih=768&biw=1472&client=firefox-b-d) s délkou 280mm