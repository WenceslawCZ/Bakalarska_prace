# Inteligentní servo

Soubory vytvořené v Altium Designer.

## Obsahuje:
* Modul s DRV8832 řídicí BLDC motor
* Enkodér AS5048A pro zjištění polohy z motoru
